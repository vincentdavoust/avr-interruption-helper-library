/*
** interrupt.h for interrupt in /home/vincent/skynavion/soft/libINT
** 
** Made by Vincent Davoust
** <vincent.davoust@gmail.com>
** 
** Started on  Wed Jan  6 17:36:22 2016 Vincent Davoust
** Last update Sun Jan 10 12:32:20 2016 Vincent Davoust
**
** Prefix : INT
*/

#ifndef INTERRUPT_H_
# define INTERRUPT_H_

// Interrupt called function pointers
void	*INT_int0_vect();
void	*INT_int1_vect();
void	*INT_int2_vect();
void	*INT_OCR0_vect();
void	*INT_OCR1A_vect();
void	*INT_OCR1B_vect();
void	*INT_OCR2_vect();
void	*INT_TOV0_vect();
void	*INT_TOV1_vect();
void	*INT_TOV2_vect();


// Ennablers
# define INT_SEI()	(SREG = (SREG | (1 << 7)))
# define INT_ENINT0()	(GICR = (GICR | (1 << 6)))
# define INT_ENINT1()	(GICR = (GICR | (1 << 7)))
# define INT_ENINT2()	(GICR = (GICR | (1 << 5)))
# define INT_ENOCR0()	(TIMSK = (TIMSK | OCIE0))
# define INT_ENOCR1A()	(TIMSK = (TIMSK | OCIE1A))
# define INT_ENOCR1B()	(TIMSK = (TIMSK | OCIE1B))
# define INT_ENOCR2()	(TIMSK = (TIMSK | OCIE2))
# define INT_ENOVF0()	(TIMSK = (TIMSK | TOIE0))
# define INT_ENOVF1()	(TIMSK = (TIMSK | TOIE1))
# define INT_ENOVF2()	(TIMSK = (TIMSK | TOIE2))

// dissableers
# define INT_UNSEI()	(SREG = (SREG & (~(1 << 7))))
# define INT_DISINT0()	(GICR = (GICR & (~(1 << 6))))
# define INT_DISINT1()	(GICR = (GICR & (~(1 << 7))))
# define INT_DISINT2()	(GICR = (GICR & (~(1 << 5))))
# define INT_DISOCR0()	(TIMSK = (TIMSK & (~OCIE0)))
# define INT_DISOCR1A()	(TIMSK = (TIMSK & (~OCIE1A)))
# define INT_DISOCR1B()	(TIMSK = (TIMSK & (~OCIE1B)))
# define INT_DISOCR2()	(TIMSK = (TIMSK & (~OCIE2)))
# define INT_DISOVF0()	(TIMSK = (TIMSK & (~TOIE0)))
# define INT_DISOVF1()	(TIMSK = (TIMSK & (~TOIE1)))
# define INT_DISOVF2()	(TIMSK = (TIMSK & (~TOIE2)))

// INT0 settings
# define INT_INT0RISE()	(MCUCR = (MCUCR | (1 << ISCO1) | (1 << ISC00)))
# define INT_INT0FALL()	(MCUCR = (MCUCR | (1 << ICS01) & (~(1 << ICS00))))
# define INT_INT0CHNG()	(MCUCR = (MCUCR | (1 << ICS00) & (~(1 << ICS01))))
# define INT_INT0LOW()	(MCUCR = (MCUCR & (~(1 << ICS01)) & (~(1 << ICS00))))

// INT1 settings
# define INT_INT1RISE()	(MCUCR = (MCUCR | (1 << ISC11) | (1 << ISC10)))
# define INT_INT1FALL()	(MCUCR = (MCUCR | (1 << ICS11) & (~(1 << ICS10))))
# define INT_INT1CHNG()	(MCUCR = (MCUCR | (1 << ICS10) & (~(1 << ICS11))))
# define INT_INT1LOW()	(MCUCR = (MCUCR & (~(1 << ICS11)) & (~(1 << ICS10))))


void INT_dummy();


#endif /* !INTERRUPT_H_ */
