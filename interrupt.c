/*
** interrupt.c for interrupt in /home/vincent/skynavion/soft/libINT
** 
** Made by Vincent Davoust
** <vincent.davoust@gmail.com>
** 
** Started on  Wed Jan  6 17:36:41 2016 Vincent Davoust
** Last update Sun Jan 10 12:32:52 2016 Vincent Davoust
**
** Prefix : INT
*/


ISR (INT0_vect) {
  *INT_int0_vect();
}

ISR (INT1_vect) {
  *INT_int1_vect();
}

ISR (INT2_vect) {
  *INT_int2_vect();
}

ISR (OCR0_vect) {
  *INT_OCR0_vect();
}

ISR (OCR1A_vect) {
  *INT_OCR1A_vect();
}

ISR (OCR1B_vect) {
  *INT_OCR1B_vect();
}

ISR (OCR2_vect) {
  *INT_OCR2_vect();
}

ISR (TOV0_vect) {
  *INT_TOV0_vect();
}

ISR (TOV1_vect) {
  *INT_TOV1_vect();
}

ISR (TOV2_vect) {
  *INT_TOV2_vect();
}

void INT_dummy() {
  return ;
}
